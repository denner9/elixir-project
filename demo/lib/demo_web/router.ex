defmodule DemoWeb.Router do
  use DemoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DemoWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/users/new", UserController, :new
  end

  # Other scopes may use custom stacks.
  scope "/api", DemoWeb do
    pipe_through :api
    resources "/users", UserController, except: [:new, :edit]
  end
end
